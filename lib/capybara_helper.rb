module CapybaraHelper
  require 'rubygems'
  require 'active_support'

  def wait_until
    require 'timeout'
    Timeout.timeout(Capybara.default_max_wait_time) do
      sleep(0.1) until value = yield
      value
    end
  end

  def element_visible?(xpath_locator)
    element_present?(xpath_locator) && element_displayed?(xpath_locator)
  end

  private

  def element_present?(xpath_locator)
    wait_until do
      begin
        page.evaluate_script('document.readyState === "complete"')
      rescue Timeout::Error
        puts 'Time out to page load'
      rescue Selenium::WebDriver::Error::JavascriptError
        puts 'Java script Error'
        retry
      end
    end
    page.has_xpath?(xpath_locator)
  end

  def element_displayed?(web_element_locator)
    find(:xpath, web_element_locator).visible?
  end

end
