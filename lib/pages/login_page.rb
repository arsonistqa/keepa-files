class LoginPage < SitePrism::Page
  element :username_field, '#username'
  element :password_field, '#login_pass'
  element :login_button, '#submitLogin'

  def login_as(email, password)
    username_field.set email
    password_field.set password
    login_button.click
    sleep 1
  end
end
