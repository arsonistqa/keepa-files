class DealsPage < SitePrism::Page

  require 'csv'

  include CapybaraHelper

  element :login_button, '#panelUserRegisterLogin'
  element :show_all_text, :xpath, '//*[@id="filterCats"]//*[contains(.,"Show all ")]'

  def click_link_in_filter(link)
    if link == 'New, 3rd Party'
      find(:xpath, "//*[@id='dealPriceTypesFilter']//*[contains(.,'#{link}')]").click
    else
      find(:xpath, "//*[@class='leftPanelBox']//*[text()='#{link}']").click
    end
    sleep 3
  end

  def select_location(location)
    find('#currentLanguage [rel="domain"]').click
    sleep 0.5
    find(:xpath, "//span[text()='#{location}']").click
    sleep 3
  end

  def select_period(period)
    find(:xpath, "//div[text()='#{period}']").click
    sleep 3
  end

  def get_links_and_save_to_file(file_name)
    number_of_all_products = show_all_text.text
    number = number_of_all_products[/\d+/].to_i

    if number < 4000
      begin
        find('#showAllDeals').click
        sleep 3
      rescue Exception => e
      end
      scroll_page(number) # scroll page to the end

      CSV.open("keepa_files/#{file_name}", 'wb') do |csv|
        add_product(csv)
      end
    else
      CSV.open("keepa_files/#{file_name}", 'wb') do |csv|
        number_of_categories = all('#filterCats span>a:first-child').size
        for i in 2..(number_of_categories + 1) do
          number_of_category_products = find(:xpath, "//*[@id='filterCats']/span[#{i}]").text
          number = number_of_category_products[/\d+/].to_i
          number = 4000 if number > 4000

          find(:xpath, "//*[@id='filterBarContent']//span[#{i}]").click
          sleep 1

          scroll_page(number)
          add_product(csv)

          back_to_top
        end
      end
    end
  end

  def scroll_page(number)
    for i in 1..(number / 80) do
      execute_script("$('html, body').animate({scrollTop:$(document).height()}, 'slow')")
      sleep 1
    end
  end

  def add_product(csv)
    all('.productContainer').each do |product|
      code = product.find('a')['href'].split('/').last
      price = product.find('.productPriceTableTdLargeS').text
      csv << [code.split('-').last, price.split(' ')[2]]
    end
  end

  def back_to_top
    execute_script("$('.backToTop').click()")
    sleep 3
    execute_script("$('html, body').animate({ scrollTop: 0 }, 'slow');")
    sleep 1
  end
end
