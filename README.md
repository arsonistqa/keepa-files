## To install all gems please use the following command

 In terminal run - ```bundle install```

## To Run tests please use the following command

### Run suite
 
 In terminal run for all features - ```cucumber```
 
### Run in parallel
 
 In terminal run for all features - ```bundle exec parallel_cucumber features/ -n 5```