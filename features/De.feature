@tests
Feature: De

  Scenario: DE - User is able to save data for Amazone
    Given I login to app as valid user
    When I select '.de' location
    And I click on 'Amazon' filter
    And I select period
    Then I get all data from search results and save to 'de_amazon.csv' file

  Scenario: DE - User is able to save data for Marketplace New
    Given I login to app as valid user
    When I select '.de' location
    And I click on 'New' filter
    And I select period
    Then I get all data from search results and save to 'de_new.csv' file

#  Scenario: DE - User is able to save data for Fulfilled by Amazone
#    Given I login to app as valid user
#    When I select '.de' location
#    And I click on 'New, Fulfilled By Amazon' filter
#    And I select period
#    Then I get all data from search results and save to 'de_fulfilled_by_amazone.csv' file
#
#  Scenario: DE - User is able to save data for 3rd Party
#    Given I login to app as valid user
#    When I select '.de' location
#    And I click on 'New, 3rd Party' filter
#    And I select period
#    Then I get all data from search results and save to 'de_3d_party.csv' file

