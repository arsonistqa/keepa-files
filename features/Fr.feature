@tests
Feature: Fr

  Scenario: FR - User is able to save data for Amazone
    Given I login to app as valid user
    When I select '.fr' location
    And I click on 'Amazon' filter
    And I select period
    Then I get all data from search results and save to 'fr_amazon.csv' file

  Scenario: FR - User is able to save data for Marketplace New
    Given I login to app as valid user
    When I select '.fr' location
    And I click on 'New' filter
    And I select period
    Then I get all data from search results and save to 'fr_new.csv' file

#  Scenario: FR - User is able to save data for Fulfilled by Amazone
#    Given I login to app as valid user
#    When I select '.fr' location
#    And I click on 'New, Fulfilled By Amazon' filter
#    And I select period
#    Then I get all data from search results and save to 'fr_fulfilled_by_amazone.csv' file
#
#  Scenario: FR - User is able to save data for 3rd Party
#    Given I login to app as valid user
#    When I select '.fr' location
#    And I click on 'New, 3rd Party' filter
#    And I select period
#    Then I get all data from search results and save to 'fr_3d_party.csv' file

