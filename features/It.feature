@tests
Feature: It

  Scenario: IT - User is able to save data for Amazone
    Given I login to app as valid user
    When I select '.it' location
    And I click on 'Amazon' filter
    And I select period
    Then I get all data from search results and save to 'it_amazon.csv' file

  Scenario: IT - User is able to save data for Marketplace New
    Given I login to app as valid user
    When I select '.it' location
    And I click on 'New' filter
    And I select period
    Then I get all data from search results and save to 'it_new.csv' file

#  Scenario: IT - User is able to save data for Fulfilled by Amazone
#    Given I login to app as valid user
#    When I select '.it' location
#    And I click on 'New, Fulfilled By Amazon' filter
#    And I select period
#    Then I get all data from search results and save to 'it_fulfilled_by_amazone.csv' file
#
#  Scenario: IT - User is able to save data for 3rd Party
#    Given I login to app as valid user
#    When I select '.it' location
#    And I click on 'New, 3rd Party' filter
#    And I select period
#    Then I get all data from search results and save to 'it_3d_party.csv' file

