$LOAD_PATH << File.dirname(__FILE__) + '/../lib'

ENVIRONMENT = (ENV['ENVIRONMENT'] || 'production').to_sym
Dir.mkdir('keepa_files') unless File.directory?('keepa_files')
unless File.exist? "#{File.dirname(__FILE__)}/../../lib/config/#{ENVIRONMENT}.yml"
  raise "Create a configuration file '#{ENVIRONMENT}.yml' under lib/config"
end

require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'selenium-webdriver'
require 'site_prism'
require 'env_config'
require 'keepa'
require 'pages'
require 'support/string'
require 'capybara_helper'
require 'active_support/all'
require 'rspec'

World(CapybaraHelper)

$period = ENV['period']

Capybara.configure do |config|
  config.default_driver = :selenium
  config.javascript_driver = :selenium

  config.run_server = EnvConfig.get :run_server
  config.default_selector = EnvConfig.get :default_selector
  config.default_max_wait_time = EnvConfig.get :wait_time

  # capybara 2.5.0 config options
  config.match = EnvConfig.get :match
  config.ignore_hidden_elements = EnvConfig.get :ignore_hidden_elements
end

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :firefox)
end

def generate_random_string(length = 6)
  string = ''
  chars = ('a'..'z').to_a
  length.times do
    string << chars[rand(chars.length - 1)]
  end
  string
end

Before do
  page.driver.browser.manage.delete_all_cookies
  page.driver.browser.manage.window.maximize
end

After do
  Capybara.current_session.driver.quit
end
