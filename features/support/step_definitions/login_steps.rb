Given(/^I navigate on keepa page$/) do
  @keepa = Keepa.new
  visit (EnvConfig.get :app_url).to_s
end

When(/^I login to app as valid user$/) do
  step 'I navigate on keepa page'

  step "I click ['Log in'] button"

  email = EnvConfig.get :username
  password = EnvConfig.get :password

  @keepa.login_page.login_as(email, password)
end
