When /^I click on '(.*)' filter$/ do |link|
  @keepa.deals_page.click_link_in_filter(link)
end

When /^I select '(.*)' location$/ do |location|
  @keepa.deals_page.select_location(location)
end

When /^I select period$/ do
  @keepa.deals_page.select_period($period)
end

Then /^I get all data from search results and save to '(.*)' file$/ do |file_name|
  @keepa.deals_page.get_links_and_save_to_file(file_name)
end