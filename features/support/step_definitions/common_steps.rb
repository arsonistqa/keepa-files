When(/^I click \['(.*)'\] button$/) do |button|
  case button
  when 'Log in'
    @keepa.deals_page.login_button.click
  else
    raise ArgumentError, "message: #{button} is not supported"
  end
end